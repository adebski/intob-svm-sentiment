#!/usr/bin/env bash
echo "classifier,dataset,pca,iteration,step_size,reg_param,mini_batch_fraction,train_milis,test_milis,accuracy" > har-test-custom.csv

sbt "run -c spark-multiclass -d har -i 1000 -s 0.5 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"

# iterations changing
sbt "run -c spark-multiclass -d har -i 10 -s 0.5 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 30 -s 0.5 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 300 -s 0.5 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 0.5 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 3000 -s 0.5 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 10000 -s 0.5 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"

# step changing
sbt "run -c spark-multiclass -d har -i 1000 -s 0.1 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 0.3 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 0.5 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 1.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 3.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 5.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 10.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 15.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"

#reg param changing
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.0 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.005 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.01 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.03 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.05 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.3 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.5 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 1.0 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 3.0 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 10.0 -m 0.5 -o har-test-custom.csv --directory har"

# mini batch changing
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.0 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.1 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.2 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.3 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.4 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.6 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.7 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.8 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.9 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 1.0 -o har-test-custom.csv --directory har"

# pca changing
sbt "run -c spark-multiclass -d har -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -p 500 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -p 300 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -p 100 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -p 70 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -p 40 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -p 20 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -p 10 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"
sbt "run -c spark-multiclass -d har -p 5 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o har-test-custom.csv --directory har"