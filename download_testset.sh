wget https://archive.ics.uci.edu/ml/machine-learning-databases/00240/UCI%20HAR%20Dataset.zip -O test-dataset.zip
unzip test-dataset.zip
mkdir -p har
mv UCI\ HAR\ Dataset/* har/
rm -r UCI\ HAR\ Dataset
rm test-dataset.zip
