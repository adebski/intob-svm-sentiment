#!/usr/bin/env bash
echo "classifier,dataset,frequency,iteration,step_size,reg_param,mini_batch_fraction,train_milis,test_milis,accuracy" > stanford-multiclass-custom2.csv
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 0.5 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 0.5 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 0.5 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 0.5 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 0.5 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 1.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 1.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 1.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 1.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 1.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 5.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 5.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 5.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 5.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 5.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 10.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 10.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 10.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 10.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 10.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 0.5 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 0.5 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 0.5 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 0.5 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 0.5 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 1.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 1.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 1.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 1.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 1.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 3.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 3.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 3.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 3.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 3.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 5.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 5.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 5.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 5.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 5.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 10.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 10.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 10.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 10.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 10.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 0.5 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 0.5 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 0.5 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 0.5 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 0.5 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 1.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 1.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 1.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 1.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 1.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 3.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 3.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 3.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 3.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 3.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 5.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 5.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 5.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 5.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 5.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 10.0 -r 0.0 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 10.0 -r 0.002 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 10.0 -r 0.005 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 10.0 -r 0.01 -m 0.5 -o stanford-multiclass-custom2.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 5000 -s 10.0 -r 0.02 -m 0.5 -o stanford-multiclass-custom2.csv"
