import re
import sys
import os

POSITIVE = 'positive'
NEGATIVE = 'negative'

sentiments_map = {
    "0": NEGATIVE,
    "1": NEGATIVE,
    "2": None,
    "3": POSITIVE,
    "4": POSITIVE
}


def prepare(sentence):
    sentiment = sentiments_map[sentence[1]]
    return ' '.join([word for word in re.split('\W+|[0-4]', sentence) if word != ""]), sentiment


def read_lines(file_name):
    with open(file_name, 'r') as file:
        return file.readlines()


def process_tree(file_name):
    processed_senteces = [prepare(line) for line in read_lines(file_name)]
    return [sentence for sentence in processed_senteces if sentence[1] is not None]


def save_to_file(file_name, lines):
    print lines
    with open(file_name, 'w+') as output_file:
        for line_tuple in lines:
            output_file.write(line_tuple[0] + '|' + line_tuple[1] + os.linesep)

            


if __name__ == '__main__':

    file_names = sys.argv[1:]

    for file_name in file_names:
        sentences = process_tree(file_name)
        save_to_file('processed_' + file_name, sentences)
