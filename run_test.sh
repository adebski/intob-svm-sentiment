#!/bin/bash

if [[ "$1" -eq "1" ]]; then
  sbt "run-main intob.SVMMain iris smo 100 -1"
elif [[ "$1" -eq "2" ]]; then
  sbt "run-main intob.SVMMain iris spark 100 -1"
elif [[ "$1" -eq "3" ]]; then
  sbt "run-main intob.SVMMain har-smaller smo 100 10"
elif [[ "$1" -eq "4" ]]; then
  sbt "run-main intob.SVMMain har-smaller spark 100 10"
elif [[ "$1" -eq "5" ]]; then
  sbt "run-main intob.SVMMain har-smaller spark 100 -1"
elif [[ "$1" -eq "6" ]]; then
  sbt "run-main intob.SVMMain har spark 100 10"
elif [[ "$1" -eq "7" ]]; then
  sbt "run-main intob.SVMMain har spark 100 -1"
fi