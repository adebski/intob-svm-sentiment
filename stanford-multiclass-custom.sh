#!/usr/bin/env bash
echo "classifier,dataset,frequency,iteration,step_size,reg_param,mini_batch_fraction,train_milis,test_milis,accuracy" > stanford-multiclass-custom.csv

# iterations changing
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 10 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 30 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 100 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 300 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 3000 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 10000 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 30000 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"

# step changing
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 0.1 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 0.3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 0.5 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 1 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 5 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 10 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 15 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 30 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"

# reg param changing
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.0 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.005 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.01 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.03 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.05 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.1 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.3 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 0.5 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 1.0 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 3.0 -m 0.5 -o stanford-multiclass-custom.csv"
sbt "run -c spark-multiclass -d stanford-multiclass -f 2 -i 1000 -s 3 -r 10.0 -m 0.5 -o stanford-multiclass-custom.csv"