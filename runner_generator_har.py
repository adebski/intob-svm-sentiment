#!/usr/bin/env python
import sys

script_file = sys.argv[1]
output_file_name = sys.argv[2]

classifier = 'spark-multiclass'
dataset = 'har'
directory = sys.argv[3]

iterations = [10, 100, 1000, 10000]
step_sizes = [0.1, 0.5, 1.0, 3.0]
reg_params = [0.1, 0.5, 1.0, 3.0]
mini_batch_fractions = [0.1, 0.5, 1.0]
pcas = [None, 5, 20, 100]

def generate_sbt_run(iteration, step_size, reg_param, mini_batch_fraction, pca):
	if (pca):
		return 'sbt "run -c {0} -d {1} -p {2} -i {3} -s {4} -r {5} -m {6} -o {7} --directory {8}"'.format(classifier, dataset, pca, iteration, step_size, reg_param, mini_batch_fraction, output_file_name, directory)
	else:
		return 'sbt "run -c {0} -d {1} -i {2} -s {3} -r {4} -m {5} -o {6} --directory {7}"'.format(classifier, dataset, iteration, step_size, reg_param, mini_batch_fraction, output_file_name, directory)

with open(script_file, "w") as f:
	f.write('#!/usr/bin/env bash\n')

	f.write('echo "classifier,dataset,pca,iteration,step_size,reg_param,mini_batch_fraction,train_milis,test_milis,accuracy" > {0}\n'.format(output_file_name))
	for iteration in  iterations:
		for step_size in step_sizes:
			for reg_param in reg_params:
				for mini_batch_fraction in mini_batch_fractions:
					for pca in pcas:
						line = generate_sbt_run(iteration, step_size, reg_param, mini_batch_fraction, pca)
						f.write(line)
						f.write('\n')

