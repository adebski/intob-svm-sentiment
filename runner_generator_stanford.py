#!/usr/bin/env python
import sys

script_file = sys.argv[1]
output_file_name = sys.argv[2]

classifier = sys.argv[3]
dataset = sys.argv[4]
frequency = sys.argv[5]
iterations = [1000, 3000, 5000]
step_sizes = [0.5, 1.0, 3.0, 5.0, 10.0]
reg_params = [0.0, 0.002, 0.005, 0.01, 0.02]
mini_batch_fractions = [0.5]

def generate_sbt_run(iteration, step_size, reg_param, mini_batch_fraction):
	return 'sbt "run -c {0} -d {1} -f {2} -i {3} -s {4} -r {5} -m {6} -o {7}"'.format(classifier, dataset, frequency, iteration, step_size, reg_param, mini_batch_fraction, output_file_name)

with open(script_file, "w") as f:
	f.write('#!/usr/bin/env bash\n')

	f.write('echo "classifier,dataset,frequency,iteration,step_size,reg_param,mini_batch_fraction,train_milis,test_milis,accuracy" > {0}\n'.format(output_file_name))
	for iteration in  iterations:
		for step_size in step_sizes:
			for reg_param in reg_params:
				for mini_batch_fraction in mini_batch_fractions:
					line = generate_sbt_run(iteration, step_size, reg_param, mini_batch_fraction)
					f.write(line)
					f.write('\n')