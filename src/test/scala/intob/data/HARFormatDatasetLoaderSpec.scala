package intob.data

import com.google.common.io.Resources
import intob.data.Util.toScalaVectors
import org.scalatest.{Matchers, WordSpec}

class HARFormatDatasetLoaderSpec extends WordSpec with Matchers {
  val testIrisDirectory = Resources.getResource("iris-test").getPath

  "HARFormatDatasetLoader" should {
    "load test data" in {
      // given
      val loader = new HARFormatDatasetLoader(testIrisDirectory)

      // when
      val result = loader.testData

      // then
      val expected = Vector(
        (Vector(5.4, 3.7, 1.5, 0.2), 1),
        (Vector(4.8, 3.4, 1.6, 0.2), 3),
        (Vector(1.0, 2.4, 4.6, 0.2), 2)
      )

      assertResult(expected)(toScalaVectors(result))
    }

    "load train data" in {
      // given
      val loader = new HARFormatDatasetLoader(testIrisDirectory)

      // when
      val result = loader.trainData

      // then
      val expected = Vector(
        (Vector(1.5, 2.5, 3.5, 4.5), 3),
        (Vector(4.5, 3.5, 1.0, 0.2), 1),
        (Vector(1.0, 2.5, 4.0, 0.6), 1)
      )

      assertResult(expected)(toScalaVectors(result))
    }
  }
}