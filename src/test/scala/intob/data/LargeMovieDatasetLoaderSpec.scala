package intob.data

import com.google.common.io.Resources
import intob.classifier.Classifier.LabelledData
import intob.data.Util.toScalaVectors
import org.scalatest.{Matchers, WordSpec}

class LargeMovieDatasetLoaderSpec extends WordSpec with Matchers {
  val largeMovieDatasetDirectory = Resources.getResource("large-movie-dataset").getPath

  "LargeMovieDatasetLoader" should {
    "prepare vector of feature words" in {
      // when
      val loader = new LargeMovieDatasetLoader(largeMovieDatasetDirectory)

      // then
      // order checked manually
      val expectedWords = Vector("second", "review", "train", "negative", "positive", "first")

      assertResult(expectedWords)(loader.featureWords)
    }

    "return train data" in {
      // given
      val loader = new LargeMovieDatasetLoader(largeMovieDatasetDirectory)

      // when
      val result: LabelledData = loader.trainData

      // then
      // Vector("second", "review", "train", "negative", "positive", "first") - if word is present in file then 1.0
      // else 0.0
      val expectedDataWithFeatures = Vector(
        (Vector(0.0, 1.0, 1.0, 1.0, 0.0, 1.0), 0),
        (Vector(1.0, 1.0, 1.0, 1.0, 0.0, 0.0), 0),
        (Vector(0.0, 1.0, 1.0, 0.0, 1.0, 1.0), 1),
        (Vector(1.0, 1.0, 1.0, 0.0, 1.0, 0.0), 1)
      )

      assertResult(expectedDataWithFeatures)(toScalaVectors(result))
    }

    "return test data" in {
      // given
      val loader = new LargeMovieDatasetLoader(largeMovieDatasetDirectory)

      // when
      val result: LabelledData = loader.testData

      // then
      // Vector("second", "review", "train", "negative", "positive", "first") - if word is present in file then 1.0
      // else 0.0
      val expectedDataWithFeatures = Vector(
        (Vector(0.0, 0.0, 0.0, 1.0, 0.0, 1.0), 0),
        (Vector(1.0, 0.0, 0.0, 1.0, 0.0, 0.0), 0),
        (Vector(0.0, 0.0, 0.0, 0.0, 1.0, 1.0), 1),
        (Vector(1.0, 0.0, 0.0, 0.0, 1.0, 0.0), 1)
      )

      assertResult(expectedDataWithFeatures)(toScalaVectors(result))
    }
  }
}