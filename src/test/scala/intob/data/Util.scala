package intob.data

import intob.classifier.Classifier

object Util {
  def toScalaVectors(sparseVectors: Classifier.LabelledData): Vector[(Vector[Double], Int)] = {
    sparseVectors.map(instance => (instance.features.toArray.toVector, instance.label)).toVector
  }
}