package intob

import org.rogach.scallop._

class ParseConfiguration(args: Seq[String]) extends ScallopConf(args) {
  banner("Sample classification application, supports SMO and Spark/Gradient implementation of SVM")

  val dataset = opt[String](required = true, descr = "name of the dataset: har, stanford, stanford-multiclass,iris, large-movie-dataset")
  val directory = opt[String](required = false, descr = "directory of the dataset if it is not default one")
  val classifier = opt[String](required = true, descr = "classifier type: spark, spark-multiclass, smo, smo-multiclass")
  val pca = opt[Int](required = false, descr = "if PCA should be used pass number of dimensions ")
  val iterations = opt[Int](required = false, descr = "number of iterations")
  val frequency = opt[Int](required = false, descr = "minimum feature word frequency for stanford/large-movie-dataset loaders")
  val stepSize = opt[Double](required = false, descr = "Step size to be used for each iteration of gradient descent in Spark SVM")
  val regParam = opt[Double](required = false, descr = "Regularization parameter for Spark SVM")
  val miniBatchFraction = opt[Double](required = false, descr = "Fraction of data to be used per iteration for Spark SVM")
  val threads = opt[Int](required = false, default = Some(4), descr = "Number of worker threads for Spark")
  val outputFile = opt[String](required = false, descr = "Name of the file to which accuracy will be written")
}