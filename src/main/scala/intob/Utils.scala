package intob

import com.typesafe.scalalogging.slf4j.StrictLogging
import intob.classifier.{Classifier, IntegerLabeledPoint}
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.Matrix

object Utils extends StrictLogging {

  /**
   * Computes PCA using Spark implementation
   * @param data
   * @param reducedFeaturesNumber number of dimensions to which feature vectors will be reduced
   * @return labeled data containing feature vectors with reduced dimension
   */
  def pca(data: Classifier.LabelledData, reducedFeaturesNumber: Int): Classifier.LabelledData = {
    val features = data.map(_.features)
    val labels = data.map(_.label)
    val featuresNumber = features(0).size

    logger.info(s"Reducing feature vectors from $featuresNumber to $reducedFeaturesNumber")

    val sc = SparkUtils.sc

    val featuresRDD = sc.makeRDD(features)
    logger.info("Created RDD from features vector")
    val matrix: RowMatrix = new RowMatrix(featuresRDD)
    logger.info("Created RowMatrix from features RDD")
    val pcaMatrix: Matrix = matrix.computePrincipalComponents(reducedFeaturesNumber)
    logger.info("Computed PCA matrix")
    val projected = matrix.multiply(pcaMatrix)
    logger.info("Multiplied rowMatrix and pcaMatrix")

    val reducedFeatures: Array[Classifier.DataType] = projected.rows.collect()

    val result = labels.zip(reducedFeatures).map(tuple => IntegerLabeledPoint(tuple._1, tuple._2))

    logger.info("PCA succeeded")

    result
  }

  /**
   *
   * @param results pairs of labels
   * @return percentage of correct guesses: label._1 == label._2
   */
  def calculateCorrectGuessPercentage(results: Array[PredictResult]): Double = {
    val numberOfCorrectGuesses = results.foldLeft(0) { (acc, predictResult) =>
      if (predictResult.expectedLabel == predictResult.predictedLabel) acc + 1 else acc
    }

    numberOfCorrectGuesses / results.size.toDouble
  }
}