package intob.classifier

import org.apache.spark.mllib.linalg.Vector

/**
 * Represents single data point that is passed to the classifiers.
 *
 * @param label Label associated with given feature vector
 * @param features Feature vector, it must be one of org.apache.spark.mllib.linalg.Vector implementations
 *                 instead of standard Scala Vector
 */
case class IntegerLabeledPoint(label: Int, features: Vector)