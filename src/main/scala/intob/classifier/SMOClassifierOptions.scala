package intob.classifier

import intob.classifier.Kernels.Kernel

/**
 * Options for SMO classifier
 *
 * @param C regularization parameter, 1/lamdba (SVM notation), the bigger the less important the bias term is
 * @param tolerance numerical tolerance of the algorithm
 * @param alphaTolerance tolerance for alpha vector elements used for optimization of the algorithm.
 *                       All support vectors less than alphaTolerance are removed.
 * @param maxIterations Maximum number of iterations.
 * @param noChangeDataMaxPasses Maximum number of iterations when there was no improvement.
 * @param kernel Similarity function used to determine a value to maximization process.
 *               We want to delineate the data using most distant line/curve.
 */
case class SMOClassifierOptions(
                                 C: Double = 1.0,
                                 tolerance: Double = 1e-4,
                                 alphaTolerance: Double = 1e-7,
                                 maxIterations: Int = 100,
                                 noChangeDataMaxPasses: Int = 10,
                                 kernel: Kernel = new LinearKernel) extends ClassifierOptions