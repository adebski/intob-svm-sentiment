package intob.classifier

import com.typesafe.scalalogging.slf4j.StrictLogging
import intob.SparkUtils
import intob.classifier.Classifier.DataType
import org.apache.spark.mllib.classification.{SVMModel, SVMWithSGD}
import org.apache.spark.mllib.regression.LabeledPoint

/**
 * Classifier that uses Spark SVM implementation
 *
 * @param options configuration options for classifier
 */
class SparkSVMClassifier(val options: SparkSVMOptions) extends Classifier with StrictLogging {

  import intob.classifier.Classifier.LabelledData

  var model: SVMModel = null

  /**
   *
   * @param labeledData should contain {0, 1} labelled data
   */
  override def train(labeledData: LabelledData): Unit = {
    logger.info("Creating Spark LabeledPoints")
    val sparseLabelledPoints = labeledData.map(instance => LabeledPoint(instance.label, instance.features))
    logger.info("finished creating Spark LabeledPoints, creating RDD")

    val sc = SparkUtils.sc
    val trainingData = sc.makeRDD(sparseLabelledPoints)

    logger.info("Caching training data")
    trainingData.cache()
    logger.info("Cached training data")

    model = SVMWithSGD.train(trainingData,
      numIterations = options.iterations,
      stepSize = options.stepSize,
      regParam = options.regParam,
      miniBatchFraction = options.miniBatchFraction)
  }

  def margin(inst: DataType): Double = {
    model.predict(inst)
  }

  /**
   *
   * @param inst feature vector to classify
   * @return one of {0, 1}
   */
  override def predict(inst: DataType): Int = {
    val probabilisticPrediction = margin(inst)

    probabilisticPrediction.round.toInt
  }
}

