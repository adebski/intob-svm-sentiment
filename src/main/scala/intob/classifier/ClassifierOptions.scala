package intob.classifier

/**
 * Base trait for all types of classifier options.
 *
 * It only serves as marker trait, because different classifiers can have many different options user will need to
 * pattern match to retrieve concrete implementation.
 */
trait ClassifierOptions