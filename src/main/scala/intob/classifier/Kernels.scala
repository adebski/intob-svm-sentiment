package intob.classifier

object Kernels {
  import intob.classifier.Classifier.DataType

  type Kernel = (DataType, DataType) => Double
}