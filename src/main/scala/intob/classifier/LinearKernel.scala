package intob.classifier

import intob.classifier.Classifier.DataType

class LinearKernel extends Kernels.Kernel {
  override def apply(v1: DataType, v2: DataType): Double = {
    var sum = 0.0

    for (idx <- 0 until v1.size) {
      sum = sum + v1(idx) * v2(idx)
    }

    sum
  }
}