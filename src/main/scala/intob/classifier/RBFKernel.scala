package intob.classifier

import intob.classifier.Classifier._
import intob.classifier.Kernels.Kernel

class RBFKernel(val sigma: Double) extends Kernel {
  override def apply(v1: DataType, v2: DataType): Double = {
    def blah(x: Double, y: Double) = ( x - y ) * ( x + y )
    var s = 0.0

    for (idx <- 0 until v1.size) {
      s = s + blah(v1(idx), v2(idx))
    }

    Math.exp(-1.0 * s / ( 2.0 * sigma * sigma ))
  }
}