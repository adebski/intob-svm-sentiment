package intob

import java.io.{FileOutputStream, File, PrintWriter}

import com.typesafe.scalalogging.slf4j.StrictLogging
import intob.classifier.{SMOClassifierOptions, SparkSVMOptions}

object SentimentMain extends App with StrictLogging {
  logger.info(s"cmd arguments are: '${args.mkString("|")}'")

  val parsedOptions = new ParseConfiguration(args)

  // hack for Spark options and lazy val
  System.setProperty("spark.threads", parsedOptions.threads().toString)

  val loaderFactory = new LoaderFactory
  val classifierFactory = new ClassifierFactory

  val loader = loaderFactory.create(parsedOptions)
  val classifier = classifierFactory.create(parsedOptions)
  val classifierOptions = classifier.options

  var trainData = loader.trainData
  logger.info(s"Loaded train data, got ${trainData.size} pairs")

  if (parsedOptions.pca.isDefined) {
    logger.info(s"Applying PCA to train dataset")
    trainData = Utils.pca(trainData, parsedOptions.pca())
  }

  val trainStart = System.currentTimeMillis();
  classifier.train(trainData)
  val trainEnd = System.currentTimeMillis()

  var testData = loader.testData
  logger.info(s"Loaded train data, got ${testData.size} pairs")

  if (parsedOptions.pca.isDefined) {
    logger.info(s"Applying PCA to test dataset")
    testData = Utils.pca(testData, parsedOptions.pca())
  }

  val testStart = System.currentTimeMillis()
  val answers = testData.map(instance => PredictResult(predictedLabel = classifier.predict(instance.features), expectedLabel = instance.label))
  val testEnd = System.currentTimeMillis()
  val accuracy = Utils.calculateCorrectGuessPercentage(answers)
  SparkUtils.sc.stop()

  logger.info(s"Accuracy = ${accuracy * 100}%")

  if (parsedOptions.outputFile.isDefined) {
    val outputStream = new FileOutputStream(new File(parsedOptions.outputFile()), true)
    val writer = new PrintWriter(outputStream)

    val classifier = parsedOptions.classifier()
    val dataset = parsedOptions.dataset()
    val trainMillis = trainEnd - trainStart
    val testMillis = testEnd - testStart

    parsedOptions.dataset() match {
      case "stanford" | "stanford-multiclass" => {
        val frequency = if (parsedOptions.frequency.isDefined) {
          parsedOptions.frequency()
        } else {
          -1
        }

        classifierOptions match {
          case SparkSVMOptions(iterations, stepSize, regParam, miniBatchFraction) => {
            writer.write(s"$classifier,$dataset,$frequency,$iterations,$stepSize,$regParam,$miniBatchFraction,$trainMillis,$testMillis,$accuracy\n")
          }
        }
      }
      case "har" | "iris" => {
        val pca = if (parsedOptions.pca.isDefined) {
          parsedOptions.pca()
        } else {
          -1
        }

        classifierOptions match {
          case SparkSVMOptions(iterations, stepSize, regParam, miniBatchFraction) => {
            writer.write(s"$classifier,$dataset,$pca,$iterations,$stepSize,$regParam,$miniBatchFraction,$trainMillis,$testMillis,$accuracy\n")
          }
          case SMOClassifierOptions(regParam, _, _, iterations, _, _) => {
            writer.write(s"$classifier,$dataset,$pca,$iterations,,$regParam,,$trainMillis,$testMillis,$accuracy\n")
          }
        }
      }
    }

    writer.close()
  }
}