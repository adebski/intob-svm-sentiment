package intob.data

import java.io.File

import intob.classifier.Classifier._

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
 * Loads binary version of stanford dataset.
 *
 * Unprocessed trees can be found here: http://nlp.stanford.edu/sentiment/trainDevTestTrees_PTB.zip
 *
 * Dataset should contain at least two files: train.txt, test.txt. Each line should be in format
 * <review>|<integer_label>
 *
 * @param datasetDirectory path to directory with test.txt and train.txt files
 * @param requiredWordFrequency minimum word frequency to be included in feature words vector
 */
class StanfordLoaderMulticlass(datasetDirectory: String = "src/main/resources/stanford_dataset_multiclass",
                               requiredWordFrequency: Int = 1) extends SentimentReviewLoader {
  private val trainReviewsFileName = new File(datasetDirectory + File.separator + "train.txt")
  private val testReviewsFileName = new File(datasetDirectory + File.separator + "test.txt")
  private val trainReviews: ReviewsData = loadReviews(trainReviewsFileName)
  private val _featureWords: Vector[String] = prepareFeatureWords(trainReviews, requiredWordFrequency)

  private def loadReviews(file: File): ReviewsData = {
    logger.info(s"Loading reviews from ${file.getAbsolutePath}")

    val arrayBuffersMap = (0 to 4).map(label => label -> ArrayBuffer[Vector[String]]()).toMap
    val reviewsBufferedSource = Source.fromFile(file)
    val reviews = reviewsBufferedSource.getLines()

    reviews.foreach(line => processReviewAddToMap(arrayBuffersMap, line))

    arrayBuffersMap.foreach { t =>
      val label = t._1
      val reviews = t._2

      logger.info(s"Loaded ${reviews.size} reviews with label $label")
    }

    reviewsBufferedSource.close()

    val resultMap = arrayBuffersMap.map(t => (t._1, t._2.toVector))
    ReviewsData(resultMap)
  }

  private def processReviewAddToMap(resultMap: Map[Int, ArrayBuffer[Vector[String]]],
                                     reviewWithLabel: String): Unit = {
    val splitLine = reviewWithLabel.split("\\|")
    val words = reviewToWords(splitLine(0))
    val label = splitLine(1).toInt

    resultMap(label).append(words)
  }

  override def featureWords: Vector[String] = _featureWords

  /**
   *
   * @return Data that will be used to train some classifier, all labels will have the same value as in train.txt
   */
  override def trainData: LabelledData = {
    val result = prepareLabelledData(trainReviews)
    logger.info("prepared train data for classifier")

    result
  }

  /**
   *
   * @return Data that will be used to train some classifier, all labels will have the same value as in test.txt
   */
  override def testData: LabelledData = {
    logger.info("starting to load test data")
    val testReviews = loadReviews(testReviewsFileName)
    logger.info("loaded to load test data")

    val result = prepareLabelledData(testReviews)
    logger.info("prepared test data for classifier")

    result
  }
}
