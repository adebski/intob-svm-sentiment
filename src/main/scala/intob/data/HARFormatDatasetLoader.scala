package intob.data

import java.nio.file.Paths

import intob.classifier.Classifier.LabelledData
import intob.classifier.{Classifier, IntegerLabeledPoint}
import org.apache.spark.mllib.linalg.Vectors

import scala.io.Source

/**
 * Loads HAR dataset: https://archive.ics.uci.edu/ml/datasets/Human+Activity+Recognition+Using+Smartphones
 *
 * Dataset base directory should contain test/train directories. In each directory there should be file
 * called X_[train|test].txt containing lines with features as double values. For each line in X_[train|test].txt file y_[train|test].txt
 * contains integer based labels.
 *
 * @param directoryName path to base directory
 */
class HARFormatDatasetLoader(directoryName: String = "iris") extends Loader {

  private val trainFeaturePath = "train/X_train.txt"
  private val trainLabelPath = "train/y_train.txt"
  private val testFeaturePath = "test/X_test.txt"
  private val testLabelPath = "test/y_test.txt"

  private val trainFeatureFileName = Paths.get(directoryName, trainFeaturePath).toString
  private val trainLabelFileName = Paths.get(directoryName, trainLabelPath).toString
  private val testFeatureFileName = Paths.get(directoryName, testFeaturePath).toString
  private val testLabelFileName = Paths.get(directoryName, testLabelPath).toString

  private val _trainData = prepareData(featuresFileName = trainFeatureFileName, labelFileName = trainLabelFileName)
  private val _testData = prepareData(featuresFileName = testFeatureFileName, labelFileName = testLabelFileName)

  private def featurize(labelsWithFeatures: (String, String)): Classifier.LabelledDataInstance = {
    val featuresString = labelsWithFeatures._1
    val label = labelsWithFeatures._2
    val features = featuresString.trim().split(whitespacesRegex).map(_.toDouble)

    IntegerLabeledPoint(label.toInt, Vectors.dense(features))
  }

  private def prepareData(featuresFileName: String, labelFileName: String): Classifier.LabelledData = {
    logger.info(s"Parsing data from featureFile = ${featuresFileName}, labelFile = ${labelFileName}")

    val featuresFile = Source.fromFile(featuresFileName)
    val labelsFile = Source.fromFile(labelFileName)
    val featuresWithLabels = featuresFile.getLines().zip(labelsFile.getLines())
    val result = featuresWithLabels.map(featurize)

    logger.info("parsed dataset")

    result.toArray
  }

  /**
   *
   * @return Data that will be used to train some classifier, all labels will have the same value as in train/y_train.txt
   */
  override def trainData: LabelledData = _trainData

  /**
   *
   * @return Data that will be used to train some classifier, all labels will have the same value as in test/y_test.txt
   */
  override def testData: LabelledData = _testData
}