package intob

import org.apache.spark.{SparkConf, SparkContext}

object SparkUtils {
  lazy val threads = System.getProperty("spark.threads").toInt
  lazy val conf = new SparkConf(false) // skip loading external settings
    .setMaster(s"local[$threads]")
    .setAppName("firstSparkApp")
    .set("spark.logConf", "false")
    .set("spark.driver.host", "localhost")
    .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
  lazy val sc = new SparkContext(conf)
}
