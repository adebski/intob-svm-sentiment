package intob.classifier;

import intob.PredictResult;
import intob.PredictResult$;
import intob.Utils$;
import intob.data.Loader;
import org.jage.address.agent.AgentAddressSupplier;
import org.jage.agent.SimpleAgent;
import org.jage.property.PropertySetter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class ClassifierAgent extends SimpleAgent {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Classifier classifierStrategy;
    private Loader loader;

    @Inject
    public ClassifierAgent(final AgentAddressSupplier supplier) {
        super(supplier);
    }

    @PropertySetter(propertyName = "classifierStrategy")
    public void setClassifierStrategy(final Classifier classifierStrategy) {
        this.classifierStrategy = classifierStrategy;
    }

    @PropertySetter(propertyName = "loader")
    public void setLoader(final Loader loader) {
        this.loader = loader;
    }

    @Override
    public void step() {
        IntegerLabeledPoint[] testData = loader.testData();
        IntegerLabeledPoint[] trainData = loader.trainData();
        logger.info("Loaded data from using " + loader);

        PredictResult[] results = new PredictResult[testData.length];
        int index = 0 ;

        classifierStrategy.train(trainData);
        logger.info("Finished training of " + classifierStrategy);

        for(IntegerLabeledPoint labeledPoint: testData) {
            int predictedLabel = classifierStrategy.predict(labeledPoint.features());

            results[index] = PredictResult$.MODULE$.apply(predictedLabel, labeledPoint.label());
            ++index;
        }

        double correctGuessPercentage = Utils$.MODULE$.calculateCorrectGuessPercentage(results);

        logger.info("Got accuracy: " + correctGuessPercentage);
    }
}
