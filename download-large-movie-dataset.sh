#!/usr/bin/env bash

wget -O large-movie-dataset.tgz http://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz
tar xvzf large-movie.dataset.tgz -C large-movie-dataset
rm large-movie-dataset.tgz